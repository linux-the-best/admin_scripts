//Выводит данные из таблицы Postgres в виде html на сайте
<?php
$user = "USER";
$pass = "PASS";
$db = "DB_NAME";

$con = pg_connect("dbname=$db user=$user password=$pass")
    or die('Could not connect to server' . pg_last_error());

$query = 'SELECT * FROM <TABLE_NAME>';
$result = pg_query($query) or die('Cannot execute query' . pg_last_error());

echo "<html>
    <body>
    <table>
        <tr>
            <th>ID</th>
            <th>title</th>
            <th>text</th>
            <th>data</th>
        </tr>";

while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
    echo "\t<tr>\n";
    foreach ($line as $col_value) {
        echo "\t\t<td>$col_value</td>\n";
    }
    echo "\t</tr>\n";
}

echo  "</table></body></htmml>";
pg_free_result($result);
pg_close($con);
?>
